/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.suphakorn.interestrateproject;

/**
 *
 * @author exhau
 */
public class InterestRate {

    private double Deposit;
    private double Rate = 0.3 / 100;

    public InterestRate(double Deposit) {
        this.Deposit = Deposit;
        this.Rate = Rate;
    }
    //calculate Interest Rate Deposit amount
    public double Cal() {
        return Rate * Deposit;
    }

    //override method toString
    @Override
    public String toString(){
        return "Interest Deposit amount is " + this.Cal();
    }
}
